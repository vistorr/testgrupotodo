<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Categoria;

class Subcategoria extends Model
{
   

    //
    //
     /**
     * Get the comments for the blog post.
     */
    public function categorias()
    {
        //return $this->hasMany(Categoria::class);
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }

}
