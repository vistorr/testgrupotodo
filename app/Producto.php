<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //

     /**
     * Get the comments for the blog post.
     */
    public function categorias()
    {
        //return $this->hasMany(Categoria::class);
        return $this->belongsTo('App\Categoria', 'id_categoria');
    }

     /**
     * Get the comments for the blog post.
     */
    public function subcategorias()
    {
        //return $this->hasMany(Categoria::class);
        return $this->belongsTo('App\Subcategoria', 'id_subcategoria');
    }
}
