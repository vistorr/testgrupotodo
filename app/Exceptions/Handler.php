<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\QueryException;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $e)
    {
        //reporta los errores
        parent::report($e);       
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        
    }



    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
       
       /*         
        if ($e instanceof NotFoundHttpException) {
            return response()->view('errors.404', [], 404);
        }
        
        if ($e instanceof \Illuminate\Database\QueryException){
            //return json_encode(array("error" => 1, "msg" => $exception));
            return response()->view('errors.general', [], 500);    
        }
        // custom error message
        if ($e instanceof Exception) {
            return response()->view('errors.general', [], 500);
        } 
        */
        //return json_encode(array('success' => true, "error" => 0, "msg" => "OK"));
        //return response()->view('errors.500', [], 500);
        if ($e instanceof ModelNotFoundException) {
            return response()->view('errors.404', [], 404);
        }
        if ($e instanceof NotFoundHttpException) {
            return response()->view('errores.404', [], 404);
        }
        return parent::render($request, $e);
              
    }
}
