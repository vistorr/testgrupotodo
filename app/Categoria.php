<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subcategoria;

class Categoria extends Model
{
    
    /**
     * Get the post that owns the comment.
     */
    public function subcategorias()
    {
        //return $this->belongsTo(Subcategoria::class);
        return $this->hasMany('App\Subcategoria', 'categoria_id');
    }
}
