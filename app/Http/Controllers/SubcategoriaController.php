<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

use App\Subcategoria;
use App\Categoria;
use App\ProductoCategoria;
use App\Producto;

class SubcategoriaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $subcategorias = Subcategoria::all();
        $categorias = Categoria::all();     
        

        return view('admin.subcategoria.index', ['subcategorias' => $subcategorias,'categorias' => $categorias]);
    }


    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'nombre'   => 'required|min:5|max:50',
            'descripcion'   => 'min:5|max:150',
            'categoria'   => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return json_encode(array("error" => 1, "msg" => "Error al guardar"));
        } else {

            try {
                // store categoria
                $subcategoria = new Subcategoria;
                $subcategoria->nombre      = $request->nombre;
                $subcategoria->descripcion = $request->descripcion; 
                $subcategoria->categoria_id = $request->categoria;
                $subcategoria->save();
                

            }catch(QueryException $e){            
                return json_encode(array("error" => 1, "msg" => $e->getMessage()));
            }catch(ModelNotFoundException $e){            
                return json_encode(array("error" => 1, "msg" => $e->getMessage()));
            }    
            catch(Exception $e){            
                return json_encode(array("error" => 1, "msg" => $e->getMessage()));             
            }    

            return json_encode(array('success' => true, "error" => 0, "msg" => "OK"));
        }
    }

    public function subcategoriaPorId($id)
    {

        $subcategoria = Subcategoria::find($id);
        return json_encode(array('success' => true, "error" => 0, "msg" => "OK", 'subcategoria' => $subcategoria));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        
        try {
            // store categoria
            $categoria = Subcategoria::find($id);
            $categoria->delete();
            

        } catch (Exception $e) {
            return json_encode(array("error" => 1, "msg" => $e->getMessage()));
        }

        return json_encode(array('success' => true, "error" => 0, "msg" => "OK"));
    }


    
    public function SubcategoriaPorIdCategoria($idcategoria){
       
        $subcategorias = Subcategoria::where('categoria_id', $idcategoria)->get();
        return json_encode(array('success' => true, "error" => 0, "msg" => "OK", 'subcategorias' => $subcategorias));
    }
}
