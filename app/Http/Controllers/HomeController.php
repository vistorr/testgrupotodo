<?php

namespace App\Http\Controllers;

use Exception;

use Illuminate\Http\Request;
use App\Categoria;
use App\Producto;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try{
            
            $categorias = Categoria::all();
            /*
            $categoria = Categoria::find(1);
            dd($categoria->subcategorias);
            */            
            return view('home', ['categorias' => $categorias]);    
            
        }catch(QueryException $e){            
            $categorias = Categoria::all();        
            return response()->view('errores.500', ['categorias' => $categorias]);            
        }catch(ModelNotFoundException $e){            
            $categorias = Categoria::all();        
            return response()->view('errores.general', ['categorias' => $categorias]);            
        }    
        catch(Exception $e){            
            $categorias = Categoria::all();        
            return response()->view('errores.general', ['categorias' => $categorias]);            
        }     
        
    }
}
