@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 text-center">
        <h1>HOME</h1>
        <div class="card mb-2 shadow">
            <div class="card-body">
                <h5 class="card-title">PRODUCTOS</h5>   
                <a href="{{route("productos")}}" class="btn btn-primary">ver</a>             
            </div>
        </div>
        <div class="card mb-2 shadow">
            <div class="card-body">
                <h5 class="card-title">CATEGORIAS</h5>     
                <a href="{{route("categorias")}}" class="btn btn-primary">ver</a>              
            </div>
        </div>
       
    </div>
</div>
@endsection
