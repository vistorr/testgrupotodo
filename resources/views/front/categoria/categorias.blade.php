@extends('layouts.app')
@section('breadcrumbs')
   <div class="d-none">
        {{Breadcrumbs::addCrumb("Home","/")}} 
        {{Breadcrumbs::addCrumb("Categorias","/Categorias")}} 
        {{Breadcrumbs::addCssClasses("breadcrumb")}} 

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item">Productos</li>                
            </ol>
        </nav>
    </div>

    {!!Breadcrumbs::render()!!} 
@endsection
@section('content')
<div class="container">
    <div class="col-md-12 text-center">
        <h1>CATEGORIAS</h1>

        <div class="accordion" id="accordionExample">
            @foreach($categorias as $item)
                
                <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$item->id}}" aria-expanded="true" aria-controls="collapse{{$item->id}}">
                            {{$item->nombre}}
                        </button>
                    </h2>
                </div>

                <div id="collapse{{$item->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        @if(count($item->subcategorias)>0 )
                            <ul class="list-group list-group-flush">
                                @foreach($item->subcategorias as $itemsubc)
                                    <li class="list-group-item"> <a class="dropdown-item" href="#"> 
                                        <i class="fas fa-angle-right"></i>
                                        {{$itemsubc->nombre}}</a>  
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>                
                
            @endforeach
            
            
        </div>
    </div>
</div>
@endsection
