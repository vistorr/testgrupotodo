@extends('layouts.app')
@section('breadcrumbs')
    <div class="d-none">
        {{Breadcrumbs::addCrumb("Home","/")}} 
        {{Breadcrumbs::addCrumb("Productos","/Productos")}} 
        {{Breadcrumbs::addCssClasses("breadcrumb")}} 

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item">Productos</li>                
            </ol>
        </nav>
    </div>

    {!!Breadcrumbs::render()!!} 
    
@endsection

@section('content')
<div class="container">
    <div class="col-md-12 text-center">
        <h1>PRODUCTOS</h1>
        
       
        
          


        <table class="table table-sm">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th> 
                    <th scope="col">Descripcion</th>   
                    <th scope="col">Categoría</th>
                    <th scope="col">Subcategoría</th>                   
                </tr>
            </thead>
            <tbody>
                @foreach($productos as $item)
                <tr>
                    <th scope="row">{{$item->id}}</th>
                    <td>{{$item->nombre}}</td>
                    <td>{{$item->descripcion}}</td>
                    <td>{{$item->categorias->nombre}}</td>
                    <td>{{$item->subcategorias->nombre}}</td>
                </tr>
                @endforeach
                
            </tbody>
        </table>

    
    
    </div>
</div>
@endsection
