<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home')}}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                
                <li class="nav-item">
                    <a class="nav-link" href="{{route('productos')}}">Productos</a>
                </li>
                
                <li class="nav-item">
                    <div class="dropdown">
                        <a class="nav-link dropdown-toggle" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Categorias
                        </a>
                        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                            <li class="dropdown-item"><a href="{{route('categorias')}}">Listado de Categorías</a></li>
                            <div class="dropdown-divider"></div>
                            @foreach($categorias as $item)                                
                                @if(count($item->subcategorias)>0 )
                                    <li class="dropdown-submenu">
                                        <a class="dropdown-item" tabindex="-1" href="#">{{$item->nombre}}</a>
                                        <ul class="dropdown-menu">
                                            @foreach($item->subcategorias as $itemsubc)
                                                <li class="dropdown-item"><a tabindex="-1" href="#">{{$itemsubc->nombre}}</a></li>   
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li class="dropdown-item"><a href="#">{{$itemsubc->nombre}}</a></li>
                                @endif
                            @endforeach    
                        </ul>
                    </div>
                </li>



                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    
                </li>
                

                @endguest
            </ul>
        </div>
    </div>
</nav>

