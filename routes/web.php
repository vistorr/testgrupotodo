<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|   ->middleware('auth');
*/

use App\Http\Controllers\ProductoController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\SubcategoriaController;


//Route::get('/', 'ProductoController@listado');
Route::get('/', function () {
    return redirect('/home');
});

Route::group(['prefix' => 'admin'], function () {

    // admin Producto
    Route::get('producto/{id}', 'ProductoController@show');
    Route::get('productos', 'ProductoController@index');
    Route::post('agregarProducto', 'ProductoController@store');
    Route::post('actualizarProducto', 'ProductoController@update');
    Route::delete('eliminarProducto/{id}', 'ProductoController@destroy');
    Route::get('productoPorId/{id}', 'ProductoController@productoPorId');


    //  admin Categoria
    Route::get('categoria/{id}', 'CategoriaController@show');
    Route::get('categorias', 'CategoriaController@index');
    Route::post('agregarCategoria', 'CategoriaController@store');
    Route::post('actualizarCategoria', 'CategoriaController@update');
    Route::delete('eliminarCategoria/{id}', 'CategoriaController@destroy');
    Route::get('buscarCategoria/{nombre}', 'CategoriaController@search');
    Route::get('categoriaPorId/{id}', 'CategoriaController@categoriaPorId');


    
     //  admin SubCategoria
     Route::get('subcategoria/{id}', 'SubcategoriaController@show');
     Route::get('subcategorias', 'SubcategoriaController@index');
     Route::post('agregarSubcategoria', 'SubcategoriaController@store');
     Route::post('actualizarSubcategoria', 'SubcategoriaController@update');
     Route::delete('eliminarSubcategoria/{id}', 'SubcategoriaController@destroy');
     Route::get('buscarSubcategoria/{nombre}', 'SubcategoriaController@search');
     Route::get('subcategoriaPorId/{id}', 'SubcategoriaController@subcategoriaPorId');
     Route::get('subcategoriaPorIdCategoria/{idcategoria}', 'SubcategoriaController@SubcategoriaPorIdCategoria');

     
    Route::get('/', function () {
    return view('greeting', ['name' => 'James']);
});
    

});

Route::group(['prefix' => 'front'], function () {

    //front producto
    Route::get('productos', 'ProductoController@listado')->name('productos');
    Route::get('producto/{id}', 'ProductoController@detalle');

    //front categoria
    Route::get('categorias', 'CategoriaController@listado')->name('categorias');
    Route::get('categoria/{id}', 'CategoriaController@detalle');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

