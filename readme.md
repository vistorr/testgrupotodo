## Doc

Pasos a seguir para instalar el Repo: 

** Levantar sobre Apache
** Configurar .env para conectarse a BD MySQL

EJECUTAR LOS SIGUIENTES COMANDOS CON COMPOSER 1.

composer update
composer install --prefer-dist

sudo chmod -R 777 storage/ bootstrap/ database/migrations/

configure database in file .env

php artisan key:generate

php artisan migrate


## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).





