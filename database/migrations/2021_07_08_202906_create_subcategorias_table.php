<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategorias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('categoria_id')->unsigned();
            $table->string('nombre');
            $table->string('descripcion')->nullable();            
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();     
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategorias');
    }
}
